# 3.7.1-2

- feat: adds `markdownlint`

# 3.7.1-1

- maintenance: pins `pre-commit` to `3.7.1`
- maintenance: updates podman base image to `v5`
- maintenance: pins `node` to `22-alpine3.19`

# 3.6.0-1

- maintenance: pins `pre-commit` to `3.6.0`
- maintenance: pins `node` to `21-alpine3.19`
- feat: passes CONTAINER_EXTRA_ARGUMENTS to all inner containers, with added `--transient-store` as default

# 3.5.0-2

- fix: fixes tflint and terraform docs

# 3.5.0-1

- maintenance: pins node default version to `21-alpine3.18`
- maintenance: pins `pre-commit` to `3.5.0`
- doc: updates CHANGELOG to new conventions
- tech: pins container to `3.5.0` to align with pre-commit
- feat: adds `relint`
- feat: (BREAKING) updates all ENV to comply with new conventions: `XXX_LOCATION`, `XXX_IMAGE_NAME` replaced `XXX_IMAGE` and `XXX_IMAGE_TAG` replace `XXX_IMAGE_VERSION`

# 1.2.0

- maintenance: pins pre-commit to `3.3.3`

# 1.1.1

- fix: unpins `git`, `python3` and `ca-certifcates` because we always want latest version and they change too often

# 1.1.0

- feat: adds `--pull newer` to all podman internal commands to take advantage of floating tags

# 1.0.1

- fix: makes sure `terrascan` command is available
- refactor: adds basic `which` test to make sure all commands are available
- doc: adjusts some documentation

# 1.0.0

- feat: uses `podman` as internal container system rather than `docker`
- feat: base image of `quay.io/podman/stable` instead of `docker:dind`
- feat: adds `hadolint` as possible dependency
- chore: adds missing `LICENSE`

# 0.5.0

- feat: adds `tfupdate` as possible dependency
- feat: adds `terrascan` as possible dependency
- feat: updates `python3` and `cacertificates` packages
- maintenance: pre-commit deps update

# 0.4.1

- fix: fixes perl image run

# 0.4.0

- maintenance: bump pre-commit to `2.20.0`
- maintenance: bump package dependencies
- feat: makes perl a sub-docker image
- refactor: micro cleaning
- doc: updates README

# 0.3.1

- fix: downgrade pre-commit to `2.17.0` because of some issues with python lib on pre-commit init

# 0.3.0

- feat: pin pre-commit to 2.18.0
- maintenance: use the new TFlint docker image since old one is now deprecated
- maintenance: bump package dependencies

# 0.2.0

- feat: install gcompat, needed for some hooks
- feat: allows the use of node
- refactor: install pre-commit as a pyz, to avoid the need of pip
- refactor: removes GCC at the end of installation
- fix: fixes shellcheck docker call

# 0.1.0

- feat: removes dev build packages after installation
- feat: allows the use of shellcheck
- chore: updates pre-commit deps
- chore: updates dependencies

# 0.0.0

- tech: initialise repository
- tech: docker pre-commit -> test OK
