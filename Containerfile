ARG IMAGE="quay.io/podman/stable"
ARG IMAGE_TAG="v5"

FROM ${IMAGE}:${IMAGE_TAG}

ARG IMAGE
ARG IMAGE_TAG
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_DESCRIPTION
ARG VCS_URL
ARG VCS_TITLE
ARG VCS_NAMESPACE
ARG CVS_REF
ARG CVS_DESCRIPTION
ARG CVS_URL
ARG CVS_TITLE
ARG CVS_NAMESPACE
ARG LICENSE
ARG VERSION
ARG CONTAINER_EXECUTABLE="podman"
ARG CONTAINER_EXTRA_ARGUMENTS="--transient-store --pull newer"

ENV PRE_COMMIT_VERSION="3.7.1" \
    HADOLINT_LOCATION="docker.io/hadolint/" \
    HADOLINT_IMAGE_NAME="hadolint" \
    HADOLINT_IMAGE_TAG="latest" \
    HADOLINT_EXE="hadolint" \
    MARKDOWNLINT_LOCATION="docker.io/markdownlint/" \
    MARKDOWNLINT_IMAGE_NAME="markdownlint" \
    MARKDOWNLINT_IMAGE_TAG="latest" \
    MARKDOWNLINT_EXE="" \
    NODE_LOCATION="docker.io/library/" \
    NODE_IMAGE_NAME="node" \
    NODE_IMAGE_TAG="22-alpine3.19" \
    NODE_EXE="" \
    PERL_LOCATION="docker.io/library/" \
    PERL_IMAGE_NAME="perl" \
    PERL_IMAGE_TAG="5" \
    PERL_EXE="perl" \
    RELINT_LOCATION="registry.gitlab.com/wild-beavers/docker/" \
    RELINT_IMAGE_NAME="relint" \
    RELINT_IMAGE_TAG="3" \
    RELINT_EXE="" \
    SHELLCHECK_LOCATION="docker.io/koalaman/" \
    SHELLCHECK_IMAGE_NAME="shellcheck-alpine" \
    SHELLCHECK_IMAGE_TAG="stable" \
    SHELLCHECK_EXE="shellcheck" \
    TERRAFORM_LOCATION="docker.io/hashicorp/" \
    TERRAFORM_IMAGE_NAME="terraform" \
    TERRAFORM_IMAGE_TAG="latest" \
    TERRAFORM_EXE="" \
    TERRAFORM_DOCS_LOCATION="quay.io/terraform-docs/" \
    TERRAFORM_DOCS_IMAGE_NAME="terraform-docs" \
    TERRAFORM_DOCS_IMAGE_TAG="latest" \
    TERRAFORM_DOCS_EXE="" \
    TERRASCAN_LOCATION="docker.io/tenable/" \
    TERRASCAN_IMAGE_NAME="terrascan" \
    TERRASCAN_IMAGE_TAG="latest" \
    TERRASCAN_EXE="" \
    TFLINT_LOCATION="ghcr.io/terraform-linters/" \
    TFLINT_IMAGE_NAME="tflint" \
    TFLINT_IMAGE_TAG="latest" \
    TFLINT_EXE="" \
    TFSEC_LOCATION="docker.io/tfsec/" \
    TFSEC_IMAGE_NAME="tfsec" \
    TFSEC_IMAGE_TAG="latest" \
    TFSEC_EXE="" \
    TFUPDATE_LOCATION="docker.io/minamijoyo/" \
    TFUPDATE_IMAGE_NAME="tfupdate" \
    TFUPDATE_IMAGE_TAG="latest" \
    TFUPDATE_EXE="" \
    CONTAINER_EXE=$CONTAINER_EXECUTABLE \
    CONTAINER_EXTRA_ARGUMENTS=$CONTAINER_EXTRA_ARGUMENTS


VOLUME /data

COPY ./resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["pre-commit"]

LABEL "org.opencontainers.image.created"=${BUILD_DATE} \
      "org.opencontainers.image.authors"="${VCS_NAMESPACE} - https://gitlab.com/wild-beavers/" \
      "org.opencontainers.image.url"="${VCS_URL}" \
      "org.opencontainers.image.documentation"="${VCS_URL}" \
      "org.opencontainers.image.source"="${VCS_URL}" \
      "org.opencontainers.image.version"="${VERSION}" \
      "org.opencontainers.image.vendor"="${VCS_NAMESPACE}" \
      "org.opencontainers.image.licenses"="${LICENSE}" \
      "org.opencontainers.image.title"="${VCS_TITLE}" \
      "org.opencontainers.image.description"="${VCS_DESCRIPTION}" \
      "org.opencontainers.image.base.name"="${IMAGE}:${IMAGE_TAG}" \

      "org.opencontainers.applications.pre-commit.version"=$PRE_COMMIT_VERSION \
      "org.opencontainers.dependencies.hadolint"="${HADOLINT_LOCATION}${HADOLINT_IMAGE_NAME}" \
      "org.opencontainers.dependencies.hadolint.version"="$HADOLINT_IMAGE_TAG" \
      "org.opencontainers.dependencies.markdownlint"="${MARKDOWNLINT_LOCATION}${MARKDOWNLINT_IMAGE_NAME}" \
      "org.opencontainers.dependencies.markdownlint.version"="$MARKDOWNLINT_IMAGE_TAG" \
      "org.opencontainers.dependencies.node"="${NODE_LOCATION}${NODE_IMAGE_NAME}" \
      "org.opencontainers.dependencies.node.version"="$NODE_IMAGE_TAG" \
      "org.opencontainers.dependencies.perl"="${PERL_LOCATION}${PERL_IMAGE_NAME}" \
      "org.opencontainers.dependencies.perl.version"="$PERL_IMAGE_TAG" \
      "org.opencontainers.dependencies.relint"="${RELINT_LOCATION}${RELINT_IMAGE_NAME}" \
      "org.opencontainers.dependencies.relint.version"="$RELINT_IMAGE_TAG" \
      "org.opencontainers.dependencies.shellcheck"="${SHELLCHECK_LOCATION}${SHELLCHECK_IMAGE_NAME}" \
      "org.opencontainers.dependencies.shellcheck.version"="$SHELLCHECK_IMAGE_TAG" \
      "org.opencontainers.dependencies.terraform"="${TERRAFORM_LOCATION}${TERRAFORM_IMAGE_NAME}" \
      "org.opencontainers.dependencies.terraform.version"="$TERRAFORM_IMAGE_TAG" \
      "org.opencontainers.dependencies.terrascan"="${TERRASCAN_LOCATION}${TERRASCAN_IMAGE_NAME}" \
      "org.opencontainers.dependencies.terrascan.version"="$TERRASCAN_IMAGE_TAG" \
      "org.opencontainers.dependencies.terraform-docs"="${TERRAFORM_DOCS_LOCATION}${TERRAFORM_DOCS_IMAGE_NAME}" \
      "org.opencontainers.dependencies.terraform-docs.version"="$TERRAFORM_DOCS_IMAGE_TAG" \
      "org.opencontainers.dependencies.tflint"="${TFLINT_LOCATION}${TFLINT_IMAGE_NAME}" \
      "org.opencontainers.dependencies.tflint.version"="$TFLINT_IMAGE_TAG" \
      "org.opencontainers.dependencies.tfsec"="${TFSEC_LOCATION}${TFSEC_IMAGE_NAME}" \
      "org.opencontainers.dependencies.tfsec.version"="$TFSEC_IMAGE_TAG" \
      "org.opencontainers.dependencies.tfupdate"="${TFUPDATE_LOCATION}${TFUPDATE_IMAGE_NAME}"\
      "org.opencontainers.dependencies.tfupdate.version"="$TFUPDATE_LOCATION"
