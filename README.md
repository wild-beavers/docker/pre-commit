# Light pre-commit container

## Build it

```bash
podman build .
```

## How to Use it

This examples shows also how to share the cache (container and pre-commit) between runs, for a local usage:

```bash
$ mkdir -p ~/.local/share/containers-pre-commit # if you want container cache
$ mkdir -p ~/.cache/pre-commit # if you want pre-commit cache
$ podman run --userns=keep-id --rm --device /dev/fuse -v $(echo $HOME)/.cache/pre-commit/:/home/podman/.cache/ -v $(echo $HOME)/.local/share/containers-pre-commit:/home/podman/.local/share/containers/ -w /data -v $(pwd):/data -t wild-beavers/docker/pre-commit run -a
```

or docker (note that `docker` it is not officially tested):

```bash
docker --privileged run --rm --device /dev/fuse -v $(echo $HOME)/.cache/pre-commit/:/home/podman/.cache/ -v $(echo $HOME)/.local/share/containers-pre-commit:/home/podman/.local/share/containers/ -w /data -v $(pwd):/data -t wild-beavers/docker/pre-commit run -a
```

or you can specify terraform version like this:

```bash
$ podman run -e TERRAFORM_IMAGE_VERSION="<version>" --userns=keep-id --rm --device /dev/fuse -v $(echo $HOME)/.cache/pre-commit/:/home/podman/.cache/ -v $(echo $HOME)/.local/share/containers-pre-commit:/home/podman/.local/share/containers/ -w /data -v $(pwd):/data -t wild-beavers/docker/pre-commit run -a
```

For the exact env variable you can change, please look at Dockerfile.

## Description

`pre-commit` podman container image build files.

By nature, pre-commit optionally depend on many dependencies.

Some hooks called by pre-commit will execute podman run commands inside a sub-container, to keep this image as small as possible.

## Sub-container binaries

- hadolint (default image pulled: `hadolint/hadolint`)
- markdownlint (default image pulled: `markdownlint/markdownlint`)
- node (default image pulled: `docker.io/library/node`)
- perl (default image pulled: `docker.io/library/perl`)
- shellcheck (default image pulled: `koalaman/shellcheck-alpine`)
- terraform (default image pulled: `hashicorp/terraform` )
- terraform-docs (default image pulled: `quay.io/terraform-docs/terraform-docs`)
- terrascan (default image pulled: `tenable/terrascan` )
- tflint (default image pulled: `wata727/tflint` )
- tfsec (default image pulled: `tfsec/tfsec`)
- tfupdate (default image pulled: `minamijoyo/tfupdate` )

### If you need to add another hooks

You will have to:

- Add a new fake binary into resources folder like the others.(look at the list before). _Don't forget to make it executable_
- Add the ENV variable according to the image needed.
